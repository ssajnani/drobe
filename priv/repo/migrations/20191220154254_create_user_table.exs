defmodule Drobe.Repo.Migrations.CreateUserTable do
  use Ecto.Migration

  def change do
    create table("user") do
      add(:creation_timestamp, :utc_datetime)
      add(:username, :string)
      add(:name, :string)
      add(:email, :string)
      add(:verified, :boolean)
      add(:verification_code, :string)
      add(:password_hash, :binary)
      add(:password_salt, :binary)
    end

    create(unique_index(:user, [:password_hash, :password_salt]))
  end
end
