# Changelog

## v1.0.0

* Enhancements
  * Updated to use Comeonin behaviour

## v0.12.0

* Changes
  * Created separate Pbkdf2 library
