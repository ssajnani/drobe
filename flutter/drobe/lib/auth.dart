import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:requests/requests.dart';

class Auth extends StatefulWidget {
  Auth({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _AuthState createState() => _AuthState();
}


class _AuthState extends State<Auth> {
  bool checkCookie = false;

  final _formKey = GlobalKey<FormState>();
  var passKey = GlobalKey<FormFieldState>();
  bool login = true;
  String name = "";
  String username = "";
  String email = "";
  String password = "";
  String error_text = "";
  String success_text = "";

  @override
  void initState() {
    Requests.get('http://localhost:4000/').then((result) {
      if (result.statusCode == 200) {
        Navigator.pushReplacementNamed(context, '/install');
      }
    });

  }

  void getAuthCode() async{
    if (login) {
      String auth_url = "http://localhost:4000/auth/login";
      String hostname = Requests.getHostname(auth_url);
      await Requests.clearStoredCookies(hostname);
      final try_auth = await Requests.post(auth_url, body: {'username': username, 'password': password}, bodyEncoding: RequestBodyEncoding.FormURLEncoded);
      if (try_auth.statusCode == 200) {
        var re = RegExp(r'(?<=_drobe_key\=)(.*)(?=\;)');
        var match = re.firstMatch(try_auth.headers['set-cookie']);
        setState(() {
          success_text = "";
          error_text = "";
        });
        await Requests.setStoredCookies(hostname, {'_drobe_key': match.group(0)});
        Requests.get('http://localhost:4000/').then((result) {
          if (result.statusCode == 200) {
            Navigator.pushReplacementNamed(context, '/install');
          }
        });
      } else {
        setState(() {
          success_text = "";
          error_text = "Error: Failed to login due to invalid credentials";
        });
      }
    } else { 
      String auth_url = "http://localhost:4000/auth/register";
      final try_registering = await Requests.post(auth_url, body: {'username': username, 'password': password, 'email': email, 'name': name}, bodyEncoding: RequestBodyEncoding.FormURLEncoded);
      if (try_registering.statusCode == 200){
        setState((){
          error_text = "";
          success_text = "Successfully registered ${username} please follow the instructions in the email sent to ${email}.";
        });

      } else {
        setState(() {
          success_text = "";
          error_text = "Error: Failed to register due to invalid information.";
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Form(
      key: _formKey,
      child: 
        Scaffold( 
        body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          children: <Widget>[
              Spacer(),
              Container(
                height: MediaQuery.of(context).size.height * 0.3,
                width: MediaQuery.of(context).size.width * 0.6,
                child: Image(image: AssetImage('images/drobe.png')),
              ),
              Spacer(),
              login ?
              Container() :
              Expanded(
                child:
                  Container(
                  width: MediaQuery.of(context).size.width * 0.70,
                  child: TextFormField(
                  // The validator receives the text that the user has entered.
                    decoration: const InputDecoration(
                      labelText: 'Name',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Name cannot be empty.';
                      } else {
                        return null;
                      }
                    },
                    onSaved: (val) {
                      setState((){
                        name = val;  
                      });
                    },
                  ),
                  ),
                flex: 1
              ),
              login ?
              Container() :
              Expanded(
                child: 
                  Container(
                  width: MediaQuery.of(context).size.width * 0.70,
                  child: TextFormField(
                  // The validator receives the text that the user has entered.
                    decoration: const InputDecoration(
                      labelText: 'Email',
                    ),
                    validator: (value) {
                      bool test_email = RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(value);
                      if (value.isEmpty) {
                        return 'Email cannot be empty.';
                      } else if (!test_email) {
                        return 'Please provide a valid email';

                      } else {
                        return null;
                      }
                    },
                    onSaved: (val) {
                      setState((){
                        email = val;  
                      });
                    },
                  ),
                  ),
                flex: 1
              ),
              Expanded(
                child: 
                  Container(
                  width: MediaQuery.of(context).size.width * 0.70,
                  child: TextFormField(
                  // The validator receives the text that the user has entered.
                    decoration: const InputDecoration(
                      labelText: 'Username',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Username cannot be empty.';
                      } else if (value.length < 6){
                        return 'Username must be 6 or more characters';

                      } else {
                        return null;
                      }
                    },
                    onSaved: (val) {
                      setState((){
                        username = val;  
                      });
                    },
                  ),
                  ),
                flex: 1
              ),
              Expanded(
                child: 
                  Container(
                  width: MediaQuery.of(context).size.width * 0.70,
                  child: TextFormField(
                  // The validator receives the text that the user has entered.
                    key: passKey,
                    obscureText: true,
                    decoration: const InputDecoration(
                      labelText: 'Password',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Password cannot be empty.';
                      } else if (value.length < 6){
                        return 'Password must be 6 or more characters';

                      } else {
                        return null;
                      }
                    },
                    onSaved: (val) {
                      setState((){
                        password = val;  
                      });
                    },
                  ),
                  ),
                flex: 1
              ),
              login ?
              Container() :
              Expanded(
                child: 
                  Container(
                  width: MediaQuery.of(context).size.width * 0.70,
                  child: TextFormField(
                  // The validator receives the text that the user has entered.
                    obscureText: true,
                    decoration: const InputDecoration(
                      labelText: 'Confirm Password',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Confirm password cannot be empty.';
                      } else if (passKey.currentState.value != value) {
                        return 'Password fields do not match.';

                      } else {
                        return null;
                      }
                    },
                  ),
                  ),
                flex: 1
              ),
              RichText(text: TextSpan(text: error_text, style: TextStyle(color: Colors.red.withOpacity(1.0)))),
              RichText(text: TextSpan(text: success_text, style: TextStyle(color: Colors.green.withOpacity(1.0)))),
              Expanded(
                child: 
                  FlatButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        getAuthCode();
                      }
                // Validate returns true if the form is valid, or false
                // Validate returns true if the form is valid, or false
                // otherwise.
              },
                    child: Text(
                      login ? "Login" : "Signup",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                flex: 1
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.05,
                child: 
                  FlatButton(
                    onPressed: () {
                      _formKey.currentState.reset();
                      setState(() {
                        login = !login;
                        error_text = "";
                        success_text = "";
                      });
                    },
                // Validate returns true if the form is valid, or false
                // otherwise.
                    child: Text(
                      login ? "Go to Signup" : "Go to Login",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ),
              ),
              Spacer()
          ],
        ),
        ),
      ),
    );
  }
}
