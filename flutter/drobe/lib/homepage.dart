import 'package:flutter/material.dart';
import 'package:requests/requests.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:collection';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
  int contributions = 0;
  bool available = true;
  bool registry = false;
  List<String> names = List<String>();
  Map<String, dynamic> groups = new Map<String, dynamic>();
  
  @override
  void initState() {
    super.initState();
    makeAvailable();
    setupSteps();
  }

  void setupSteps() async {
    setState(() {
      names = List<String>();
    });
    await getGroups();
    getCurrentRunners();
    await setupRunners();
    await authorizeRegistry();
  }

  void makeAvailable() async {
    setState(() {
      available = true;
    });
    await Requests.post('http://localhost:4000/available'); 
  }

  void authorizeRegistry() {
    var process = Process.runSync('gcloud', ['auth', "print-access-token"]);
    if (process.exitCode == 0) {
      setState(() {
        registry = true;
      });
      var token = process.stdout;
      var utf8_token = utf8.encode(token);
      var base_64_token = base64.encode(utf8_token);
      Process.start('sh', []).then((process) {
        process.stdout
        .transform(utf8.decoder)
        .listen((data) { print(data); });
        process.stderr
        .transform(utf8.decoder)
        .listen((data) { print(data); });
        process.stdin.writeln("sed -i '' '/elir-.*/{N;/environment.*/! a\\'\$'\\n\\t''environment = [\"DOCKER_AUTH_CONFIG={\\\\\"auths\\\\\":{\\\\\"gcr.io\\\\\":{\\\\\"auth\\\\\":\\\\\"${base_64_token}\\\\\"}}}\"]'\$'\\n'';}' ~/.gitlab-runner/config.toml");
        process.stdin.writeln('exit');

      });
    } else { 
      setState(() {
        registry = false;
      });
    }
  }

  void makeUnavailable() async {
    setState(() {
      available = false;
    });
    await Requests.post('http://localhost:4000/unavailable');    

  }

  void getGroups() async {
    var result = await Requests.get('http://localhost:4000/authorized-group-info');   
    setState(() {
      groups = result.json();
    });
  }

  void getCurrentRunners() {
    List<String> new_names = List<String>();
    var process = Process.runSync('gitlab-runner', ['list']);
    LineSplitter ls = new LineSplitter();
    List<String> lines = ls.convert(process.stderr);
     
    for (var i = 0; i < lines.length; i++) {
      print(lines[i]);
      if (lines[i].startsWith('elir-')) {
        var name = lines[i].split(" ").first;
        print(name);
        var parts = name.split('-');
        var elir_name = parts.sublist(1).join('-').trim();
        new_names.add(elir_name);
      }   
    }
    setState(() {
      names = new_names;
    });
  }
  
  void setupRunners() async {
    groups.forEach((k,v) {
      if (!names.contains(k)) {
        Process.start('gitlab-runner', ['register']).then((process) { 
          process.stdout
          .transform(utf8.decoder)
          .listen((data) { print(data); });
          process.stdin.writeln(v[0]);            
          process.stdin.writeln(v[1]);
          process.stdin.writeln("elir-" + k);
          process.stdin.writeln("");
          process.stdin.writeln("docker");
          process.stdin.writeln("ruby:2.6");
          
        });
      }
    }); 
    
  }

  bool notNull(Object o) => o != null;

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = groups.entries.map((group) {
          var test = Text(
              group.key,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold),
            );
          return test;
          
    }).toList(); 
      
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
              Spacer(),
              Expanded(
                child: Image(image: AssetImage('images/elir_logo_transparent.png')),
                flex: 2
              ),
              Spacer(),
              Expanded(
                child: Column(
                      children:
                        <Widget>[Text(
                          "Your runner is a part of the following groups:",
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )] + widgets +
                        <Widget>[
                          Spacer(),
                          Text(
                            "\n$contributions build contributions",
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Spacer(),
                          !registry ?
                            Text(
                              "Google Cloud Registry is not setup, please install gcloud and login.",
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )
                          : null 
                        ].where(notNull).toList()
                    ),
                flex: 1
              ),
              Spacer()
          ] 
        ), 
      ),
    );
  }
}
