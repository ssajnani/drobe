use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# We don't run a server during test. If one is required,
# you can enable the server option below.
config :drobe, DrobeWeb.Endpoint,
  http: [port: 4002],
  server: false

config :drobe, environment: :test
# Print only warnings and errors during test
config :logger, level: :warn
