import Config
### File is for runtime configuration

# Required for Phoenix to load port at runtime https://dev.to/ilsanto/deploy-a-phoenix-app-with-docker-stack-1j9c
config :drobe, DrobeWeb.Endpoint, http: [:inet6, port: System.get_env("PORT")]
