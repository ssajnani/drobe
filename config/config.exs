# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :drobe, Drobe.Repo,
  database: "drobe_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"

config :drobe, app_url: "http://localhost:4000"

# In your config/config.exs file
config :drobe, Drobe.Mailer,
  adapter: Bamboo.SMTPAdapter,
  server: "smtp.gmail.com",
  hostname: "gmail.com",
  port: 587,
  # or {:system, "SMTP_USERNAME"}
  username: "drobeappsender@gmail.com",
  # or {:system, "SMTP_PASSWORD"}
  password: {:system, "SMTP_PASSWORD"},
  # can be `:always` or `:never`
  tls: :always,
  # or {:system, "ALLOWED_TLS_VERSIONS"} w/ comma seprated values (e.g. "tlsv1.1,tlsv1.2")
  allowed_tls_versions: [:tlsv1, :"tlsv1.1", :"tlsv1.2"],
  # can be `true`
  ssl: false,
  retries: 5,
  # can be `true`
  no_mx_lookups: true,
  # can be `:always`. If your smtp relay requires authentication set it to `:always`.
  auth: :always

config :drobe,
  ecto_repos: [Drobe.Repo]

# Configures the endpoint
config :drobe, DrobeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mn3IOxHXlEAVZtCyMR1S3RQDLd/VslO9KpBkryg9f/F1st5aWCfuptio6up3wPcc",
  render_errors: [view: DrobeWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Drobe.PubSub,
  live_view: [signing_salt: "Rb4mztcF"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
