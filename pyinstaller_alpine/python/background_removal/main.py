from non_person import remove_background
from person import remove_background_person
from keras.models import load_model
import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == "person":
            img_str = input()
            model = load_model('main_model.hdf5', compile=False)
            while img_str != "exit":

                remove_background_person(img_str, model)
                img_str = input()
        else: 
            img_str = input()
            remove_background(img_str)
    else:
        print("Please provided a valid mode: person or non-person")



