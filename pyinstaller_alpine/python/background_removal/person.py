from tensorflow.keras import backend
import base64
from keras.models import load_model
import theano.ifelse
from io import BytesIO
from scipy.misc import imresize
from PIL import Image
import numpy as np
import tensorflow as tf
import cv2

# Load the pre-trained model
# provide main_model.hdf5 / main_model_2.hdf5 for the name of model


def remove_background_person(img_str, model):
    graph = tf.compat.v1.get_default_graph()
    def predict(image):
        with graph.as_default():
            # Make prediction
            prediction = model.predict(image[None, :, :, :])
        prediction = prediction.reshape((224,224, -1))
        return prediction
    img_str = base64.decodestring(img_str.encode())
    tempBuff = BytesIO()
    tempBuff.write(img_str)
    tempBuff.seek(0)
    image = Image.open(tempBuff)
    image1 = imresize(image, (224, 224)) / 255.0

    prediction = predict(image1[:, :, 0:3])

    prediction = imresize(prediction[:, :, 1], (image.height, image.width))
    prediction[prediction>0.5*255] = 255
    prediction[prediction<0.5*255] = 0

    transparency = np.append(np.array(image)[:, :, 0:3], prediction[: , :, None], axis=-1)
    png = Image.fromarray(transparency)
    png.save("output.png", "PNG") 
    output = BytesIO()
    format = 'PNG' # or 'JPEG' or whatever you want
    image.save(output, format)
    contents = output.getvalue()
    print(base64.b64encode(contents).decode())
    output.close()
    tempBuff.close()
