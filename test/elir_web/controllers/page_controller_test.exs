defmodule DrobeWeb.PageControllerTest do
  use DrobeWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert conn.status == 400
    assert conn.resp_body == "Unable to continue"
  end
end
