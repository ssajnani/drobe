FROM bitwalker/alpine-elixir-phoenix:1.10.3 as builder

ENV PORT=4000

ENV LANG=en_US.UTF-8 \
  HOME=/opt/app/ \
  TERM=xterm

# add support for development/debugging
RUN apk update && apk upgrade && apk add --no-cache bash
RUN apk add bash-doc
RUN apk add bash-completion
RUN ls

# Install Hex+Rebar
RUN mix local.hex --force && \
  mix local.rebar --force

WORKDIR /opt/app

ENV MIX_ENV=prod

COPY . $HOME/
# Run mix setup 
RUN mix deps.get 
RUN cd assets && npm install && yarn deploy
RUN cd ..
RUN mix compile
RUN mix phx.digest
RUN mix release 

FROM bitwalker/alpine-elixir-phoenix:1.10.3

LABEL maintainer="internal.tools@loblaw.ca"

ENV LANG=en_US.UTF-8 \
  HOME=/opt/app \
  TERM=xterm

WORKDIR $HOME

EXPOSE 4000

#GRAB mix release binary
COPY --from=builder $HOME/_build/ ./_build
RUN chown -R default .

WORKDIR $HOME/_build/prod/rel/elir/bin

USER default

# Run Migrations and then start Mercury
ENTRYPOINT ./migrate.sh; ./elir start
CMD ["foreground"]
