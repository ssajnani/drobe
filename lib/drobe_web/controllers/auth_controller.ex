defmodule DrobeWeb.AuthController do
  @moduledoc """
  Auth controller responsible for handling Ueberauth responses
  """
  alias Drobe.UserLib.VerifyUser
  alias Drobe.UserLib.UserFunctions

  use DrobeWeb, :controller

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "You have been logged out!")
    |> clear_session()

    # |> redirect(to: "/")
  end

  def register(conn, %{
        "username" => user,
        "password" => pass,
        "name" => name,
        "email" => email
      }) do
    case UserFunctions.create_user(user, email, pass, name) do
      {:ok, ver_code} ->
        Drobe.Mailer.send_email_for_verification(email, ver_code, user)

        conn
        |> send_resp(200, "success")

      {:error, error} ->
        send_resp(conn, 400, "#{inspect(error)}") |> halt()
    end
  end

  def verify(conn, %{"code" => code, "user" => user}) do
    case VerifyUser.verify_code(user, code) do
      true ->
        conn
        |> put_flash(:info, "Successfully Verified User #{user}. You may now login to Drobe.")
        |> send_resp(200, "Successfully verified you can now login to Drobe.")
        |> redirect(to: "/")

      false ->
        conn
        |> put_flash(:info, "Failed to Verify User #{user}.")
        |> send_resp(400, "Failed verification, please contact the helpline.")
        |> redirect(to: "/")
    end
  end

  def login(conn, %{"username" => user, "password" => pass}) do
    IO.inspect(conn)

    case VerifyUser.verify(user, pass) do
      {{true, :existent}, _, _} ->
        conn
        |> put_flash(:info, "Successfully authenticated.")
        |> put_session(:authenticated, true)
        |> put_session(:authenticated_user, user)
        |> put_session(:session_timeout_at, new_session_timeout_at(86400))
        |> configure_session(renew: true)
        # Map.get(conn.cookies, "_elir_key"))
        |> send_resp(200, "success")

      _ ->
        conn
        |> send_resp(401, "Failed to authenticate")
    end
  end

  defp now do
    DateTime.utc_now() |> DateTime.to_unix()
  end

  defp new_session_timeout_at(timeout_after_seconds) do
    now() + timeout_after_seconds
  end
end
