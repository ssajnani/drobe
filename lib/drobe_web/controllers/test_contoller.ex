defmodule DrobeWeb.TestController do
  @moduledoc """
  Auth controller responsible for handling Ueberauth responses
  """

  use DrobeWeb, :controller

  def test(conn, _params) do
    IO.inspect(conn.cookies)
    redirect(conn, to: "/")
  end
end
