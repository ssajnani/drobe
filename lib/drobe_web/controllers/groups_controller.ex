defmodule DrobeWeb.GroupsController do
  @moduledoc """
  Auth controller responsible for handling Ueberauth responses
  """
  alias Drobe.Groups

  use DrobeWeb, :controller

  def groups(conn, _params) do
    conn
    |> put_status(200)
    |> json(Groups.get_groups())
  end

  def authorized_group_info(conn, _params) do
    user =
      conn
      |> get_session(:current_user)

    IO.inspect(conn.cookies)

    conn
    |> put_status(200)
    |> json(Groups.get_authorized_groups_info(conn |> get_session(:current_user)))
  end

  def add_group(conn, _params) do
    Groups.add_group(conn.body_params)

    conn
    |> send_resp(200, "Successfully added group")
  end

  def edit_group(conn, _params) do
    Groups.edit_group(conn.body_params)

    conn
    |> send_resp(200, "Successfully edited group")
  end
end
