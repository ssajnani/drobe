defmodule DrobeWeb.StatusController do
  @moduledoc """
  Auth controller responsible for handling Ueberauth responses
  """

  use DrobeWeb, :controller

  def available(conn, _params) do
    conn
    |> put_session(:available, true)
    |> send_resp(200, "Successfully updated")
  end

  def unavailable(conn, _params) do
    conn
    |> put_session(:available, false)
    |> send_resp(200, "Successfully updated")
  end
end
