defmodule Drobe.Plugs.CheckAuth do
  @moduledoc """
  Check authentication status of session
  """
  require Logger
  import Plug.Conn

  def init(default \\ :ok), do: default

  def call(conn, _args) do
    IO.inspect(conn)
    conn = conn |> fetch_session() |> Phoenix.Controller.fetch_flash()

    case conn
         |> get_session(:authenticated) do
      true ->
        conn

      _ ->
        stop_conn(conn, 400, "Unable to continue")
    end
  end

  defp stop_conn(conn, code, msg) do
    conn
    |> send_resp(code, msg)
    |> halt()
  end
end
