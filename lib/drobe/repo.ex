defmodule Drobe.Repo do
  use Ecto.Repo,
    otp_app: :drobe,
    adapter: Ecto.Adapters.Postgres

  import Logger

  @doc """
  Validates the changeset provided and inserts the item into a database
  schema is a generic ecto schema type
  schema_struct should be an empty struct defined by the ecto schema
  """
  @spec schema_validate_insert(map, any(), any()) :: {:ok, any()} | {:error, any()}
  def schema_validate_insert(params, schema, schema_struct) do
    changeset = schema.changeset(schema_struct, params)
    insert_item(changeset)
  end

  # Inserts a single entry recall to the database.
  # Returns the output of inserting changeset and logs the attempt.
  def insert_item(changeset) do
    try do
      case insert(changeset) do
        {:ok, struct} ->
          Logger.info("Successfully logged #{changeset.__struct__} with id #{inspect(struct.id)}")
          {:ok, struct}

        {:error, changeset} ->
          Logger.warn(
            "Error inserting #{changeset.__struct__} with data: \n\n#{inspect(changeset)}\n\n due to: #{
              cs_to_string(changeset)
            }"
          )

          {:error, changeset}
      end
    rescue
      e in Ecto.ConstraintError -> {:error, e}
    end
  end

  @doc """
  Turns a changeset.error to a string
  """
  def cs_to_string(changeset) do
    changeset
    |> Ecto.Changeset.traverse_errors(fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
    |> Enum.reduce("", fn {k, v}, acc ->
      errs = Enum.join(v, "; ")
      "#{acc}#{k} #{errs}"
    end)
  end
end
