defmodule Drobe.UserLib.VerifyUser do
  import Ecto.Query, only: [from: 2]
  alias Drobe.Repo
  alias Drobe.Crypto.Hashing
  alias Drobe.UserLib.UserFunctions

  def verify(user, pass) do
    case Drobe.Schemas.User
         |> Repo.get_by(username: user) do
      nil ->
        {false, :non_existent}

      record_map ->
        hash = record_map.password_hash
        salt = record_map.password_salt

        {
          {
            Hashing.verify_hash(pass, salt, record_map.password_hash) and
              record_map.verified == true,
            :existent
          },
          salt,
          hash
        }
    end
  end

  def verify_code(user, code) do
    case Drobe.Schemas.User
         |> Repo.get_by(username: user) do
      nil ->
        false

      record_map ->
        if record_map.verified == false and record_map.verification_code == code do
          UserFunctions.update_verification_status(user)
          true
        else
          false
        end
    end
  end
end
