defmodule Drobe.UserLib.UserFunctions do
  alias Drobe.UserLib.VerifyUser
  alias Drobe.Schemas.User, as: USER
  alias Drobe.Repo
  alias Drobe.Crypto.Hashing

  def set_change_password(user_id, old_password, new_password) do
    ver_result = VerifyUser.verify(user_id, old_password)

    case ver_result do
      {{true, :existent}, hash, salt} ->
        hash_insert_replace_password(new_password, user_id, hash, salt)
        {:ok, "New password was successfully set"}

      {{false, :existent}, _, _} ->
        {:error, "Incorrect password was provided"}

      {false, :non_existent} ->
        {:error, "Invalid username provided"}
    end
  end

  def hash_insert_replace_password(new_pw, id, old_hash, old_salt) do
    {hash, salt} = Hashing.generate_hash_salt(new_pw)
    update_user_password(id, old_hash, old_salt, hash, salt)
  end

  def get_user(username, format \\ true) do
    case Drobe.Schemas.User
         |> Repo.get_by(username: username) do
      nil ->
        {:error, "User does not exist"}

      user ->
        {:ok, user}
    end
  end

  def create_user(id, email, password, name) do
    case VerifyUser.verify(id, password) do
      {false, :non_existent} ->
        {hash, salt} = Hashing.generate_hash_salt(password)
        verification_code = Pbkdf2.Base.django_salt(100)

        case Repo.schema_validate_insert(
               %{
                 username: id,
                 name: name,
                 email: email,
                 creation_timestamp: DateTime.truncate(DateTime.utc_now(), :second),
                 verified: false,
                 verification_code: verification_code,
                 password_hash: hash,
                 password_salt: salt
               },
               USER,
               %USER{}
             ) do
          {:ok, _} ->
            {:ok, verification_code}

          {:error, error} ->
            {:error, error}
        end

      _ ->
        {:error, "Failed because user exists"}
    end
  end

  def update_user_password(id, old_hash, old_salt, hash, salt) do
    Drobe.Schemas.User
    |> Repo.get_by(username: id, password_hash: old_hash, password_salt: old_salt)
    |> Ecto.Changeset.change(%{password_hash: hash, password_salt: salt})
    |> Drobe.Repo.update()
  end

  def update_verification_status(id) do
    Drobe.Schemas.User
    |> Repo.get_by(username: id)
    |> Ecto.Changeset.change(%{verified: true})
    |> Drobe.Repo.update()
  end
end
