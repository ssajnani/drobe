defmodule Drobe.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  import Supervisor.Spec

  def start(_type, _args) do
    children = [
      {Drobe.Repo, []},
      # Start the Ecto repository
      # Start the Telemetry supervisor
      # Start the PubSub system
      {Phoenix.PubSub, name: Drobe.PubSub},
      # Start the Endpoint (http/https)
      DrobeWeb.Endpoint
      # Start a worker by calling: Drobe.Worker.start_link(arg)
      # {Drobe.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Drobe.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    DrobeWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
