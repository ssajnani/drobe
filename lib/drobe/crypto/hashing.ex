defmodule Drobe.Crypto.Hashing do
  def generate_hash_salt(pin) do
    salt = Pbkdf2.Base.django_salt(12)
    opts = [digest: :sha256, format: :django]
    hash = Pbkdf2.Base.hash_password(pin, salt, opts)
    {hash, salt}
  end

  def verify_hash(password, salt, hash) do
    opts = [digest: :sha256, format: :django]
    gen_hash = Pbkdf2.Base.hash_password(password, salt, opts)
    hash == gen_hash
  end
end
