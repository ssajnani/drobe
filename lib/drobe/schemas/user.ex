defmodule Drobe.Schemas.User do
  @moduledoc """
  Schema and validations for recall_entry table
  """
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %{
          :creation_timestamp => Date.t(),
          :username => String.t(),
          :name => String.t(),
          :email => String.t(),
          :verified => boolean(),
          :verification_code => String.t(),
          :password_hash => binary(),
          :password_salt => binary()
        }

  schema "user" do
    field(:creation_timestamp, :utc_datetime)
    field(:username, :string)
    field(:name, :string)
    field(:email, :string)
    field(:verified, :boolean)
    field(:verification_code, :string)
    # pin hash
    field(:password_hash, :binary)
    # pin salt
    field(:password_salt, :binary)
  end

  # for automated entries we dont need to double check input because, we can trust code
  def changeset(pin, params) do
    pin
    |> cast(params, [
      :username,
      :name,
      :email,
      :verified,
      :verification_code,
      :password_hash,
      :password_salt
    ])
    |> validate_length(:username, min: 1, message: "Cannot be empty")
    |> validate_length(:email, min: 1, message: "Cannot be empty")
    |> validate_length(:verification_code, min: 1, message: "Cannot be empty")
    |> validate_length(:password_hash, min: 1, message: "Cannot be empty")
    |> validate_length(:password_salt, min: 1, message: "Cannot be empty")
  end
end
